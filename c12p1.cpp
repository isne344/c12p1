#include<iostream>
#include<string>

using namespace std;
template <class T>

void swap_values(T& variable1, T& variable2){
T temp;
temp = variable1;
variable1 = variable2;
variable2 = temp;
}

template <class T>
int index_of_smallest(const T a[], int start_index, int number_used)
{
	int min = a[start_index];
	
	int index_of_min = start_index;
	
	for (int index = start_index + 1; index < number_used; index++){
		if (a[index] < min)
		{
			min = a[index];
			index_of_min = index;
		}
	}	
	return index_of_min;
}
template <class T>
void sort(T a[], int number_used)
{
	int index_of_next_smallest;
	for(int index = 0; index < number_used -1 ; index++)
	{
		index_of_next_smallest = index_of_smallest(a,index,number_used);
		swap_values(a[index], a[index_of_next_smallest]);
	}
	
}

int main(){
	
	int a=1;
	int b=2;
	
	swap_values(a,b);
	
	cout<<a<<" "<<b<<endl;	
	char i='1';
	char j='2';
	swap_values(i,j);
	
	cout<<i<<" "<<j<<endl;
	int index=5;		
	int p[1000]={5,4,3,2,1};
	sort(p,index);
	
	for(int y=0;y<index;y++){
	 	cout<<p[y]<<"  ";
	 }
	
	
	return 0;	
}
